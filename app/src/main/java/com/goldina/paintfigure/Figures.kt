package com.goldina.paintfigure

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import java.util.*
import kotlin.math.pow

class Figures(val tittle: String,
              var p1: Float,
              var p2: Float,
              var p3: Float,
              var p4: Float = 0F,
              private val myPaint: Paint=Paint()
) {

    fun swapColor() {
        val rnd = Random()
        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        myPaint.color = color
    }

    fun draw(canvas: Canvas?) {
        when (tittle) {
            "circle" -> {
                canvas?.drawCircle(p1, p2, p3, myPaint)
            }
            "square" -> {
                canvas?.drawRect(p1, p2, p3, p4, myPaint)
            }
        }


    }

    fun checkFigure(x: Float, y: Float):Boolean {
        var check=false
        when (tittle) {
            "circle" -> {
                val dx = (x - p1).toDouble().pow(2)
                val dy = (y - p2).toDouble().pow(2)
                 if (dx + dy < p3.toDouble().pow(2)) {
                   check= true
                }
            }
            "square" -> {
                if(x in p1..p3 && y in p2..p4) {
                   check = true
                }
            }
        }
        return check
    }
}
