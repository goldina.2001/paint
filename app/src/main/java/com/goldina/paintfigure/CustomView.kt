package com.goldina.paintfigure

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class CustomView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0,
) : View(context, attrs, defStyleAttr) {
    private var listFigures = mutableListOf<Figures>()

    fun addFigure(figure: Figures){
        listFigures.add(figure)
        invalidate()
    }
    fun deleteAll(){
        listFigures.clear()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if(listFigures.isNotEmpty()){
            for(figure in listFigures){
                figure.draw(canvas)
                postInvalidate()
            }

        }

    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val value = super.onTouchEvent(event)
         when(event?.action){
             MotionEvent.ACTION_DOWN ->{
                 return true
             }
             MotionEvent.ACTION_MOVE->{
                 val x = event.x
                 val y =event.y
                 for(figure in listFigures){
                     if(figure.checkFigure(x,y)){
                         val dx = figure.p3 - figure.p1
                         val dy = figure.p4 - figure.p2
                         figure.p1 =x
                         figure.p2=y
                         if(figure.tittle=="square") {
                             figure.p3 = dx + x
                             figure.p4 = dy + y
                         }
                         postInvalidate()
                     }
                 }
                 return true
             }
         }
        return value
    }

}