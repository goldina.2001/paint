package com.goldina.paintfigure

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.goldina.paintfigure.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.bottomNavigationView.setOnItemSelectedListener {
            when(it.itemId){
                R.id.circle->{
                    val circle = Figures("circle",400F,400F,100F)
                    circle.swapColor()
                    binding.customView.addFigure(circle)
                }
                R.id.square->{
                    val square = Figures("square",100F,100F,200F,200F)
                    square.swapColor()
                    binding.customView.addFigure(square)
                }
                R.id.clear->{
                    binding.customView.deleteAll()
                }
            }
            true
        }

    }
}